<style>
html, body, * {
  align: justify;
}
</style>
# Visualization of Sets

<div style="text-align: center">
![UpSetChartCover][UpSetChartCover]
</div>
As most already know, Mathematica provides a wide range of visualizations with great presets for viewing one's data. However, its excellence does not preclude it from putative improvement.

Anyone who has wanted to make even a simple Venn-Diagram in Mathematica has probably sought out one of the following resources:

1. [How to plot Venn Diagrams][How to plot Venn Diagrams]

  a. [Via RegionPlots (Updated from MathWold)][Updated MathWorld RegionPlot Venn Diagram]

  b. [Via Cosine approach][Cosine Venn Diagrams]

  c. [With set based Cardinality (not scaled to intersection elements)][DrMajorBob Venn Diagram with Cardinality]

2. [Tried pasting their sets into WolframAlpha][WolframAlpha 5 Sets] (although this [breaks][WolframAlpha 6 Sets] for more than 5 sets)

and even then, these solutions are more hot-fixes for such a fundamental chart type.

Furthermore, even if Venn-Diagram's were implemented in Mathematica the scale poorly. With $2^n$ comparisons needed to be shown, they can get out of hand fast. Just consider this Venn-Digram published in Nature that shows the relationship between the banana’s genome and the genome of five other species:


<div style="text-align: center">
![Bad Venn Diagram][Bad Venn Diagram]
</div>


Notice, that is hard to identify which comparison belongs to what region and the size of these regions have no relationship to cardinality of the subset represented there.


# UpSetChart

The UpSetChart is a new twist on visualizing comparisons between sets:


<div style="text-align: center">
![UpSetChart Concept][UpSetChart Concept]
</div>

Here, each region of the Venn-Diagram is given it's own row in this "indicator grid", where each column represents a set.

In combination with two bar charts - one for set cardinalities and the other for comparison cardinality - it becomes clear how elements of sets are related to one another:



```
sets = <|
  "a" -> {7, 77, 53, 95, 42, 41},
  "b" -> {51, 88, 87, 67, 90, 37, 96},
  "c" -> {15, 87, 99, 6, 20, 87, 98, 68},
  "d" -> {46, 85, 6, 90},
  "e" -> {72, 97, 15, 55, 87}
|>;


UpSetChart[sets, "DropEmpty" -> False, "ComparisonSortBy" -> "Name", "SetSortBy" -> "Name", Axes->{True, True}]
```

<div style="text-align: center">
![UpSetChart1][UpSetChart1]
</div>

While this is nice (and in the Notebook has useful tooltips), we already are starting to see that this is going to scale poorly.

Image if we had $20$ sets! For $20$ sets there are $1,048,576$ comparisons that would have to be visualized. Dropping empty sets, and sorting by cardinality allows for a clear overview of how these sets are related:


```
randomSets = RandomData[20];
UpSetChart[randomSets, "DropEmpty" -> True,
  "SetSortBy" ->  "Cardinality",
  "ComparisonSortBy" -> "Cardinality"
]

```
<div style="text-align: center">
![UpSetChart2][UpSetChart2]
</div>

By removing the empty comparisons, we are still left with a lot of comparisons to look at. Although this is better than a traditional Venn-Diagram we make our data even more accessible. If we group our comparisons by the number of sets being used to make the comparison, the chart becomes far more manageable:

```
UpSetChart[
  RandomData[20],
  "DropEmpty" -> True,
  "SetSortBy" -> "Cardinality",
  "ComparisonSortBy" -> "Cardinality",
  "TabbedByComparisonsDegreeQ" -> True,
  "ImageSize" -> {Automatic, 250}
]
```


<div style="text-align: center">
![UpSetTabbed][UpSetTabbed]
</div>

in gif form:
<div style="text-align: center">
![UpSetGif][UpSetGif]
</div>


While this is no means a perfect solution, it is a step in the right direction.

The UpSetChart package can be found [here][Repo].
There are several styling options. However, milage may very as the axes are sort of hacked in there.




  [Repo]:https://gitlab.com/SumNeuron/UpSetChart

  [UpSetChart1]:http://community.wolfram.com//c/portal/getImageAttachment?filename=slice1.png&userId=1123546

  [UpSetChart2]: http://community.wolfram.com//c/portal/getImageAttachment?filename=20sets.png&userId=1123546


  [UpSetChartCover]: http://community.wolfram.com//c/portal/getImageAttachment?filename=slice2.png&userId=1123546

  [UpSetGif]: http://community.wolfram.com//c/portal/getImageAttachment?filename=tabbed.gif&userId=1123546

  [UpSetTabbed]: http://community.wolfram.com//c/portal/getImageAttachment?filename=tabbed1.png&userId=1123546


  [UpSetChart Concept]:http://community.wolfram.com//c/portal/getImageAttachment?filename=matrix.png&userId=1123546


  [Bad Venn Diagram]: http://community.wolfram.com//c/portal/getImageAttachment?filename=banana.png&userId=1123546

[DrMajorBob Venn Diagram with Cardinality]:https://mathematica.stackexchange.com/a/2555/42578

[How to plot Venn Diagrams]: https://mathematica.stackexchange.com/questions/2554/how-to-plot-venn-diagrams-with-mathematica?noredirect=1&lq=1


[Cosine Venn Diagrams]: https://mathematica.stackexchange.com/a/117666/42578

[Updated MathWorld RegionPlot Venn Diagram]: https://mathematica.stackexchange.com/a/2557/42578

[WolframAlpha 5 Sets]:https://www.wolframalpha.com/input/?i=A+intersect+B+intersect+C+intersect+D+intersect+E
[WolframAlpha 6 Sets]:https://www.wolframalpha.com/input/?i=A+intersect+B+intersect+C+intersect+D+intersect+E+intersect+F
