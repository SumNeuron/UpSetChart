BeginPackage["UpSetChart`OptionValidation`"]


Begin["`Private`"]
PositiveQ[number_]:= NumberQ@number && number>=0
SortQuiz[sortby_]:=MemberQ["Name", "Cardinality"}, sortby]
ColorQuiz[gradient_]:=MemberQ[ColorData["Gradients"], gradient]

ImageSizeQuiz[size_]:=
size === Automatic ||
( ListQ@size &&
  Length@size==2 &&
  AllTrue[
    size,
    PositiveQ[#] || # === Automatic &
  ]
)


AxesQuiz[axes_]:=
ListQ@axes &&
AllTrue[axes, BooleanQ] &&
Length@axes == 2

SpacerQuiz[spacer_]:=
ListQ@spacer &&
AllTrue[spacer, PositiveQ[#]&] &&
Length@spacer == 2

FontSizeQuiz Positive


ValidateOptions[options_]:=Module[
  {
    errorQ, msg, keys = Keys[options],
    key
  },


  {errorQ, msg} =
  Catch[
    Switch[key
      SetSortBy || ComparisonSortBy,
      SortQuiz[options[key]],

      ColorGradient,
      ColorQuiz[options[key]],

      DropEmpty || Verbose || TabbedByComparisonsDegreeQ,
      BooleanQ[options[key]],

      ImageSize,
      ImageSizeQuiz[options[key]],

      Axes,
      AxesQuiz[options[key]]

      Spacer,
      SpacerQuiz[options[key]],

      Radius || LineThickness || AspectRatio,
      PositiveQ[options[key]],

      IndicatorFill || IndicatorEmpty,
      ColorQ[options[key]],

      FontSize,
      Automatic===options[key] || PositiveQ[options[key]],


    ]
  ]


]

End[]
EndPackage[]
